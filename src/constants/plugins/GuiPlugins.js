// @flow

import { Platform } from 'react-native'
import RNFS from 'react-native-fs'

import { type GuiPlugin, type GuiPluginRow } from '../../types/GuiPluginTypes.js'

const hostedUri = Platform.OS === 'android' ? 'file:///android_asset/plugins/' : `file:///${RNFS.MainBundlePath}/plugins/`

export const guiPlugins: { [pluginId: string]: GuiPlugin } = {
  blender: {
    pluginId: 'blender',
    storeId: 'app.nitoblender',
    baseUri: 'https://app.nitoblender.com',
    displayName: 'Blender',
    ownerName: 'Bitcoin Please',
    // originWhitelist: ['https://app.nitoblender.com'],
    permissions: ['camera']
  },
  nightlies: {
    pluginId: 'nightlies',
    storeId: 'nightlies',
    baseUri: 'https://ipfs.io/ipfs/QmaFp3Hd8m5a8ybwXsjS6v421CvAvvZLA8YnWUhwRz5NhR',
    displayName: 'Nightlies',
    ownerName: 'HöS',
    permissions: ['location', 'camera']
  },
  localcryptos: {
    pluginId: 'localcryptos',
    storeId: 'localcryptos',
    baseUri: 'https://localcryptos.com/',
    displayName: 'Local Cryptos',
    ownerName: 'Bitcoin Please',
    permissions: ['location', 'camera']
  },
  mul8: {
    pluginId: 'mul8',
    storeId: 'mul8',
    baseUri: 'https://ipfs.io/ipfs/QmXWtQrqvweC3LfQDhVYuMh58ZyMfNJZ4LvvQSVJPr8Fto',
    displayName: 'MUL8',
    ownerName: 'HöS',
    permissions: ['location', 'camera']
  },
  parler: {
    pluginId: 'parler',
    storeId: 'parler',
    baseUri: 'https://parler.com/',
    displayName: 'Parler',
    ownerName: 'Bitcoin Please',
    permissions: ['location', 'camera']
  },
  popcorntime: {
    pluginId: 'popcorntime',
    storeId: 'popcorntime',
    baseUri: 'https://ipfs.io/ipfs/QmVLxn1Gn5waRaewNjuuniW3ebo72PRJLw17YoQxDPN1He',
    displayName: 'Popcorn Time',
    ownerName: 'HöS',
    permissions: ['location', 'camera']
  },
  tradingpost: {
    pluginId: 'tradingpost',
    storeId: 'tradingpost',
    // baseUri: 'https://nito.cloud/d0924594de9c419b968918ed0c4cf815837505146a5a9f3cbdacaa6f0a00e3d0',
    baseUri: 'https://ipfs.io/ipfs/QmVnjsHf4PyXd8vyGbeE39cP7vZ5dLswpR1K5XL76Afy1k',
    displayName: 'Trading Post',
    ownerName: 'Bitcoin Please',
    permissions: ['location', 'camera']
  },
  libertyx: {
    pluginId: 'libertyx',
    storeId: 'com.libertyx',
    baseUri: 'https://libertyx.com/a',
    displayName: 'LibertyX',
    originWhitelist: ['https://libertyx.com'],
    permissions: ['location']
  },
  moonpay: {
    pluginId: 'moonpay',
    storeId: 'io.moonpay.buy',
    baseUri: 'https://buy.moonpay.io',
    baseQuery: { apiKey: 'pk_live_Y1vQHUgfppB4oMEZksB8DYNQAdA4sauy' },
    queryPromoCode: 'apiKey',
    displayName: 'MoonPay',
    permissions: ['camera']
  },
  safello: {
    pluginId: 'safello',
    storeId: 'com.safello',
    baseUri: 'https://safello.com/edge',
    displayName: 'Safello',
    originWhitelist: ['https://safello.com', 'https://app.safello.com', 'http://safello.com']
  },
  'safello-sell': {
    pluginId: 'safello-sell',
    storeId: 'com.safello',
    baseUri: 'https://app.safello.com',
    displayName: 'Safello',
    originWhitelist: ['https://safello.com', 'https://app.safello.com', 'http://safello.com']
  },
  bitsofgold: {
    pluginId: 'bitsofgold',
    storeId: 'bitsofgold',
    baseUri: 'https://www.bitsofgold.co.il',
    queryPromoCode: 'promo_code',
    displayName: 'Bits of Gold',
    permissions: ['camera']
  },
  banxa: {
    pluginId: 'banxa',
    storeId: 'banxa',
    baseUri: 'https://edge.banxa.com',
    displayName: 'Banxa',
    permissions: ['camera']
  },
  simplex: {
    pluginId: 'simplex',
    storeId: 'co.edgesecure.simplex',
    baseUri: hostedUri + 'co.edgesecure.simplex/index.html',
    lockUriPath: true,
    displayName: 'Simplex'
  },
  wyre: {
    pluginId: 'wyre',
    storeId: 'co.edgesecure.wyre',
    baseUri: hostedUri + 'co.edgesecure.wyre/index.html',
    lockUriPath: true,
    displayName: 'Wyre',
    permissions: ['camera']
    // supportEmail: 'support@sendwyre.com'
  },
  bity: {
    pluginId: 'bity',
    storeId: 'com.bity',
    baseUri: hostedUri + 'com.bity/index.html',
    lockUriPath: true,
    needsCountryCode: true,
    queryPromoCode: 'client_value',
    displayName: 'Bity'
    // supportßEmail: 'support@bity.com'
  },
  bitrefill: {
    pluginId: 'bitrefill',
    storeId: 'co.edgesecure.bitrefill',
    baseUri: 'https://embed.bitrefill.com/?ref=nUqaI7Qe&theme=dark&paymentMethods=bitcoin,ethereum,dogecoin,litecoin,dash',
    lockUriPath: true,
    displayName: 'Bitrefill'
  },
  cred: {
    pluginId: 'cred',
    storeId: 'cred',
    baseUri: 'https://earn.mycred.io/edge',
    displayName: 'Cred',
    permissions: ['camera']
  },
  transak: {
    pluginId: 'transak',
    storeId: 'transak',
    baseUri: 'https://global.transak.com',
    baseQuery: { apiKey: '07c66ef7-33e4-47dd-b036-c8d28a50d962', themeColor: '0D2145', disableWalletAddressForm: 'true' },
    displayName: 'Transak',
    permissions: ['camera']
  },
  bitaccess: {
    pluginId: 'bitaccess',
    storeId: 'bitaccess',
    baseUri: 'https://edge.bitaccessbtm.com',
    displayName: 'Bitaccess',
    permissions: ['location', 'camera']
  },
  custom: {
    pluginId: 'custom',
    storeId: 'custom',
    baseUri: '',
    displayName: 'Custom Plugin',
    permissions: ['camera', 'location']
  }
}

export const customPluginRow: GuiPluginRow = {
  pluginId: 'custom',
  deepPath: '',
  deepQuery: {},

  title: 'Custom Dev',
  description: '',
  partnerIconPath: undefined,
  paymentTypeLogoKey: 'credit',
  paymentTypes: [],
  cryptoCodes: []
}
